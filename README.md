# Mastweepy

Mastweepy is a Python project aiming to emulate `tweepy` for Mastodon.
Ultimately, the objective is to be able to directly port *any* bot and
application written for Twitter using `tweepy` to Mastodon, without
chaging a line of code.


