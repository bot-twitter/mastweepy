import datetime

from .user import User

class Status:

    def __init__(self, masto_data, masto_api, mt_api, include_entities = True, trim_user = False, reblog_by = None):
        self.__masto_api    = masto_api
        self.__mt_api       = mt_api
        self.__masto_status = masto_data

        if masto_data['reblog'] is not None:
            reblog_by = masto_data['reblog']['account']
            masto_data = masto_data['reblog']

        self.author                    = User(masto_data['account'], masto_api, mt_api)             if reblog_by                  is None else User(reblog_by, masto_api, mt_api)
        self.created_at                = masto_data['created_at'].astimezone(datetime.timezone.utc)
        self.favorite_count            = masto_data['favourites_count']                             if reblog_by                  is None else 0
        self.favorited                 = masto_data['favourited']                                   if reblog_by                  is None else False
        self.text                      = masto_data['content']                                      if reblog_by                  is None else f"RT @{reblog_by['acct']}: {masto_data['content']}"
        self.full_text                 = masto_data['content']                                      if reblog_by                  is None else f"RT @{reblog_by['acct']}: {masto_data['content']}"
        self.id                        = masto_data['id']                                           if reblog_by                  is None else Status.id_to_retweet_id(masto_data['id'])
        self.id_str                    = str(masto_data['id'])                                      if reblog_by                  is None else str(self.id)
        self.in_reply_to_status_id     = masto_data.get('in_reply_to_id', None)                     if reblog_by                  is None else None
        self.in_reply_to_status_id_str = None                                                       if self.in_reply_to_status_id is None else str(self.in_reply_to_status_id)
        self.in_reply_to_user_id       = masto_data.get('in_reply_to_account_id', None)             if reblog_by                  is None else None
        self.in_reply_to_user_id_str   = None                                                       if self.in_reply_to_user_id   is None else str(self.in_reply_to_user_id)
        self.lang                      = masto_data['language']
        self.retweet_count             = masto_data['reblogs_count']                                if reblog_by                  is None else 0
        self.retweeted                 = masto_data['reblogged']                                    if reblog_by                  is None else False

        self.contributors              = None
        self.coordinates               = None
        self.display_text_range        = [0, 0 if self.full_text is None else len(self.full_text)]
        self.geo                       = None
        self.is_quote_status           = False # @TODO check if a toot has been linked in the message
        self.place                     = None
        self.source                    = "Unknown" if masto_data['application'] is None else masto_data['application']
        self.source_url                = "https://docs.joinmastodon.org/entities/Status/#application-website"
        self.truncated                 = False

        if include_entities:
            self.entities = {'hashtags': [], 'symbols': [], 'user_mentions': [], 'urls': []} # @TODO parse the HTML content

        if not trim_user:
            self.user = User(masto_data['account'], masto_api, mt_api)

        if reblog_by is not None:
            self.retweeted_status = Status(masto_data, masto_api, mt_api, include_entities = include_entities, trim_user = trim_user)

        if self.in_reply_to_status_id is not None:
            parent = masto_api.status(masto_data['in_reply_to_id'])
            self.in_reply_to_screen_name = parent['account']['acct']


    def destroy(self, **kwargs):
        kwargs['id'] = self.id
        self.__mt_api.destroy_status(**kwargs)


    def favorite(self, **kwargs):
        kwargs['id'] = self.id
        self.__mt_api.create_favorite(**kwargs)


    def retweet(self, **kwargs):
        kwargs['id'] = self.id
        self.__mt_api.retweet(**kwargs)


    def retweets(self, **kwargs):
        kwargs['id'] = self.id
        self.__mt_api.get_retweets(**kwargs)


    @staticmethod
    def id_to_retweet_id(status_id):
        return status_id * 10000

    @staticmethod
    def retweet_id_to_id(retweet_id):
        return retweet_id / 10000

