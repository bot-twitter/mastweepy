import datetime

class User:

    def __init__(self, masto_data, masto_api, mt_api):
        self.__masto_api              = masto_api
        self.__mt_api                 = mt_api
        self.__masto_account          = masto_data

        self.id                       = masto_data['id']
        self.id_str                   = str(masto_data['id'])
        self.screen_name              = masto_data['acct']
        self.created_at               = masto_data['created_at'].astimezone(datetime.timezone.utc)
        self.default_profile_image    = masto_data['avatar_static'].endswith("/avatars/original/missing.png")
        self.description              = masto_data['note']
        self.followers_count          = masto_data['followers_count']
        self.friends_count            = masto_data['following_count']
        self.listed_count             = len(self.list_ownerships()) if self.id == mt_api.my_id else 0
        self.name                     = masto_data['display_name']
        self.profile_banner_url       = masto_data['header_static']
        self.profile_image_url        = masto_data['avatar_static']
        self.profile_image_url_https  = masto_data['avatar_static']
        self.protected                = masto_data['locked']
        self.status                   = None #; raise NotImplemented("@TODO fetch the last status")
        self.statuses_count           = masto_data['statuses_count']

        relationships                 = masto_api.account_relationships(masto_data['id'])
        self.follow_request_sent      = relationships[0]['requested']
        self.following                = relationships[0]['following']

        self.contributors_enabled     = False
        self.default_profile          = True
        self.entities                 = {'description': {'urls': []}}
        self.favourites_count         = 0
        self.geo_enabled              = False
        self.has_extended_profile     = False
        self.is_translation_enabled   = False
        self.is_translator            = False
        self.lang                     = None
        self.location                 = ''
        self.needs_phone_verification = False
        self.notifications            = False
        self.suspended                = False
        self.time_zone                = None
        self.translator_type          = 'none'
        self.url                      = None
        self.utc_offset               = None
        self.verified                 = False
        self.withheld_in_countries    = []

        self.profile_background_color           = "000000"
        self.profile_background_image_url       = None
        self.profile_background_image_url_https = None
        self.profile_background_tile            = False
        self.profile_link_color                 = "1DA1F2"
        self.profile_location                   = None
        self.profile_sidebar_border_color       = "C0DEED"
        self.profile_sidebar_fill_color         = "DDEEF6"
        self.profile_text_color                 = "333333"
        self.profile_use_background_image       = True



    def follow(self):
        self.__mt_api.follows(self.__masto_account['acct'])
        self.following = True


    def follower_ids(self, **kwargs):
        kwargs['user_id'] = self.id
        return self.__mt_api.get_follower_ids(**kwargs)


    def followers(self, **kwargs):
        kwargs['user_id'] = self.id
        return self.__mt_api.get_followers(**kwargs)


    def friends(self, **kwargs):
        kwargs['user_id'] = self.id
        return self.__mt_api.get_friends(**kwargs)


    def list_memberships(self, **kwargs):
        kwargs['user_id'] = self.id
        return self.__mt_api.get_list_memberships(**kwargs)


    def list_ownerships(self, **kwargs):
        kwargs['user_id'] = self.id
        return self.__mt_api.get_list_ownerships(**kwargs)


    def list_subscriptions(self, **kwargs):
        kwargs['user_id'] = self.id
        return self.__mt_api.get_list_subscriptions(**kwargs)


    def lists(self, **kwargs):
        kwargs['user_id'] = self.id
        return self.__mt_api.get_lists(**kwargs)


    def timeline(self, **kwargs):
        kwargs['user_id'] = self.id
        return self.__mt_api.user_timeline(**kwargs)


    def unfollow(self, **kwargs):
        kwargs['user_id'] = self.id
        return self.__mt_api.destroy_friendship(**kwargs)


