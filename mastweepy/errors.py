import tweepy

class Error:
    def __init__(self, status_code, reason, error):
        self.status_code = status_code
        self.reason      = reason
        self.json        = lambda: {"errors": [error]}


class BadRequest(Error):
    def __init__(self, error):
        super().__init__(400, "Bad Request", error)


class NotFound(Error):
    def __init__(self, error):
        super().__init__(404, "Not Found", error)
