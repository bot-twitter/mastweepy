from mastodon import Mastodon

# Fix API
class FixedMastodon(Mastodon):
    def account_search(self, q, limit=None, following=False, offset=0, resolve=False):
        params = self._Mastodon__generate_params(locals())

        if params["following"] == False:
            del params["following"]

        if params["resolve"] == False:
            del params["resolve"]

        if params["offset"] == 0:
            del params["offset"]

        return self._Mastodon__api_request('GET', '/api/v1/accounts/search', params)

