import mastodon
import os

from .fix import FixedMastodon as Mastodon
from .user import User
from .status import Status
from .list import List
from . import errors

class API:

    def __init__(self, base_url, user_cred_file, client_cred_file = None,
                 email = None, password = None, app_name = None):

        if not os.path.exists(user_cred_file):
            if (client_cred_file is None or email is None or password is None):
                raise errors.BadRequest("215 - Bad Authentication data.")

            if not os.path.exists(client_cred_file):
                if app_name is None:
                    raise errors.BadRequest("215 - Bad Authentication data.")

                Mastodon.create_app(
                        app_name,
                        api_base_url = base_url,
                        to_file      = client_cred_file)

            mastodon = Mastodon(
                client_id    = client_cred_file,
                api_base_url = base_url)

            mastodon.log_in(
                email, password,
                to_file = user_cred_file)

        self.__masto_api = Mastodon(
            access_token = user_cred_file,
            api_base_url = base_url
        )

        self.my_masto       = self.__masto_api.me()
        self.my_id          = self.my_masto['id']
        self.my_screen_name = self.my_masto['acct']


    def verify_credentials(self, **kwargs):
        if 'include_entities' in kwargs:
            print('warning: `include_entities` has no equivalent in Mastodon and will be ignored.')

        if 'skip_status' in kwargs:
            print('warning: `skip_status` has no equivalent in Mastodon and will be ignored.')

        if 'include_email' in kwargs:
            print('warning: `include_email` has no equivalent in Mastodon and will be ignored.')

        return User(self.__masto_api.account_verify_credentials(), self.__masto_api, self)


    def get_follower_ids(self, **kwargs):
        ff    = self.get_followers(**kwargs)
        dtype = str if kwargs.get('stringify_ids', False) else int

        if 'cursor' in kwargs:
            pprev = ff._pagination_prev if hasattr(ff, "_pagination_prev") else None
            pnext = ff._pagination_next if hasattr(ff, "_pagination_next") else None

            return ([dtype(x['id']) for x in ff], (pprev, pnext))

        return [dtype(x['id']) for x in ff]


    def get_followers(self, **kwargs):
        if 'include_user_entities' in kwargs:
            print('warning: `include_user_entities` has no equivalent in Mastodon and will be ignored.')

        if 'skip_status' in kwargs:
            print('warning: `skip_status` has no equivalent in Mastodon and will be ignored.')

        user_id = self.__resolve_user_id(**kwargs, default = self.my_id)
        if 'cursor' in kwargs:
            if kwargs['cursor'] == -1:
                users = self.__repeat_to_length(
                            user_id = user_id,
                            method  = self.__masto_api.account_followers,
                            count   = kwargs.get("count", 20))
            else:
                users = self.__repeat_to_length(
                            user_id    = user_id,
                            method     = self.__masto_api.account_followers,
                            count      = kwargs.get("count", 20),
                            pagination = kwargs['cursor'])
        else:
            users = self.__repeat_to_length(
                        user_id = user_id,
                        method  = self.__masto_api.account_followers,
                        count   = kwargs.get("count", 20))[0]

        return [User(x, self.__masto_api, self) for x in users]


    def get_friends(self, **kwargs):
        if 'include_user_entities' in kwargs:
            print('warning: `include_user_entities` has no equivalent in Mastodon and will be ignored.')

        if 'skip_status' in kwargs:
            print('warning: `skip_status` has no equivalent in Mastodon and will be ignored.')

        user_id = self.__resolve_user_id(**kwargs, default = self.my_id)
        if 'cursor' in kwargs:
            if kwargs['cursor'] == -1:
                users = self.__repeat_to_length(
                            user_id = user_id,
                            method  = self.__masto_api.account_following,
                            count   = kwargs.get("count", 20))
            else:
                users = self.__repeat_to_length(
                            user_id    = user_id,
                            method     = self.__masto_api.account_following,
                            count      = kwargs.get("count", 20),
                            pagination = kwargs['cursor'])
        else:
            users = self.__repeat_to_length(
                        user_id = user_id,
                        method  = self.__masto_api.account_following,
                        count   = kwargs.get("count", 20))[0]

        return [User(x, self.__masto_api, self) for x in users]


    def get_list_memberships(self, **kwargs):
        print('warning: It is not possible to know whether or not a user has been added to a list on Mastodon')
        return []


    def get_list_ownerships(self, **kwargs):
        if 'user_id' in kwargs and kwargs['user_id'] != self.my_id:
            print("warning: Only the list of the logged-in user can be retrieved.")
            return []

        if 'screen_name' in kwargs and kwargs['screen_name'] != self.my_screen_name:
            print("warning: Only the list of the logged-in user can be retrieved.")
            return []

        if 'cursor' in kwargs:
            return ([List(x, self.__masto_api) for x in self.__masto_api.lists()], (None, None))

        return [List(x, self.__masto_api) for x in self.__masto_api.lists()]


    def get_list_subscriptions(self, **kwargs):
        print('warning: It is not possible to subscribe to someone else\'s list on Mastodon')
        return []


    def get_lists(self, **kwargs):
        return self.get_list_subscriptions(**kwargs) + self.get_list_ownerships(**kwargs)


    def user_timeline(self, **kwargs):
        parameters = {"id":       self.__resolve_user_id(**kwargs, default = self.my_id),
                      "exclude_replies": kwargs.get("exclude_replies", False),
                      "exclude_reblogs": not kwargs.get("include_rts", True),
                      "max_id":   kwargs.get("max_id", None),
                      "since_id": kwargs.get("since_id", None),
                      "limit":    kwargs.get("count", 20)}

        return [Status(x, self.__masto_api, self) for x in self.__masto_api.account_statuses(**parameters)]


    def destroy_friendship(self, **kwargs):
        user_id = self.__resolve_user_id(**kwargs, default = None)
        if user_id is None:
            raise errors.NotFound("34 - Sorry, that page does not exist.")

        return User(self.__masto_api.account(user_id), self.__masto_api, self)


    def destroy_status(self, **kwargs):
        if 'id' not in kwargs:
            raise errors.NotFound("144 - No status found with that ID.")

        try:
            status = self.__masto_api.status_delete(kwargs['id'])
            return Status(status, self.__masto_api, self)
        except mastodon.errors.MastodonNotFoundError:
            raise errors.NotFound("144 - No status found with that ID.")


    def create_favorite(self, **kwargs):
        if 'id' not in kwargs:
            raise errors.NotFound("144 - No status found with that ID.")

        try:
            status = self.__masto_api.status_favourite(kwargs['id'])
            return Status(status, self.__masto_api, self, include_entities = kwargs.get('include_entities', True))
        except mastodon.errors.MastodonNotFoundError:
            raise errors.NotFound("144 - No status found with that ID.")


    def retweet(self, **kwargs):
        if 'id' not in kwargs:
            raise errors.NotFound("144 - No status found with that ID.")

        try:
            status = self.__masto_api.status_reblog(kwargs['id'])
            return Status(status, self.__masto_api, self, trim_user = kwargs.get('trim_user', False))
        except mastodon.errors.MastodonNotFoundError:
            raise errors.NotFound("144 - No status found with that ID.")


    def retweets(self, **kwargs):
        if 'id' not in kwargs:
            raise errors.NotFound("144 - No status found with that ID.")

        try:
            status = self.__masto_api.status(kwargs['id'])
            rb_by  = self.__masto_api.status_reblogged_by(kwargs['id'])
            return [Status(status, self.__masto_api, self, trim_user = kwargs.get('trim_user', False), reblog_by = rb) for rb in rb_by[:kwargs.get('count', 20)]]
        except mastodon.errors.MastodonNotFoundError:
            raise errors.NotFound("144 - No status found with that ID.")


    def __repeat_to_length(self, *args,  method, count, pagination = None, **kwargs):
        if 'limit' in kwargs:
            del kwargs['limit']

        result   = []
        firstrun = True
        while len(result) < count:
            left = count - len(result)

            if pagination is None:
                ret = method(*args, limit = left, **kwargs)
            else:
                ret = self.__masto_api.fetch_next(pagination)

            if ret is None:
                break

            result += [x for x in ret]

            pnext = ret._pagination_next if hasattr(ret, "_pagination_next") else None
            if firstrun:
                pprev    = ret._pagination_prev if hasattr(ret, "_pagination_prev") else None
                firstrun = False

            if pnext is None:
                break

            pagination = pnext
            pagination['limit'] = count - len(result)

        pagination['limit'] = count

        return (result, (pprev, pagination))


    def __resolve_user_id(self, *, default = None, **kwargs):
        if 'user_id' in kwargs:
            return int(kwargs['user_id'])
        elif 'screen_name' in kwargs:
            try:
                account = self.__masto_api.account_lookup(kwargs['screen_name'])
                return account['id']
            except mastodon.errors.MastodonNotFoundError:
                account = self.__masto_api.account_search(kwargs['screen_name'], resolve = True, limit = 1)
                if len(account) == 0:
                    raise errors.NotFound("34 - Sorry, that page does not exist.")

                return account[0]['id']

        return default
